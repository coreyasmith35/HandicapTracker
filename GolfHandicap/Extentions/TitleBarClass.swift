//
//  titleBarClass.swift
//  GolfHandicap
//
//  Created by Corey Smith on 9/10/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit

class TitleBar: UIView {
    
    override func awakeFromNib() {
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 1.0
    }
 
}
