//
//  DoubleExt.swift
//  GolfHandicap
//
//  Created by Corey Smith on 9/5/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import Foundation

extension Double{
    func roundToTens() -> Int{
        return 10 * Int(Darwin.round(self / 10.0))
    }
    
    func roundToHundreds() -> Int{
        return 100 * Int(Darwin.round(self / 100.0))
    }
    
    func truncate(places : Int)-> Double
    {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
    
}

