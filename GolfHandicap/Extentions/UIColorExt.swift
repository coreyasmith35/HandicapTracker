//
//  UIColorExt.swift
//  GolfHandicap
//
//  Created by Corey Smith on 9/9/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import Foundation

import UIKit

class ColorClass: UIColor {
    
    func primaryColor() -> UIColor{
        return #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
    }
    
}
