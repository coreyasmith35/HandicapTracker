//
//  ButtonClass.swift
//  
//
//  Created by Corey Smith on 8/18/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit

class ButtonClass: UIButton {
    override func awakeFromNib() {
        layer.cornerRadius = 15
        layer.shadowOpacity = 0.05
        layer.opacity = 0.9
    }
    
}

class SortButtonClass: UIButton {
    override func awakeFromNib() {
        layer.cornerRadius = 12
        layer.shadowOpacity = 0.02
        layer.opacity = 0.95
    }
    
}

class DatePickerButtonClass: UIButton {
    override func awakeFromNib() {
        layer.cornerRadius = 5
        layer.borderWidth = 0.25
        layer.borderColor = #colorLiteral(red: 0.6391444206, green: 0.6392566562, blue: 0.6391373277, alpha: 1)
    }
    
}

class InfoButton: UIButton {
    override func awakeFromNib() {
        self.tintColor = UIColor.white
    }
    
    override public func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let relativeFrame = self.bounds
        let hitTestEdgeInsets = UIEdgeInsetsMake(-18, -18, -18, -18)
        let hitFrame = UIEdgeInsetsInsetRect(relativeFrame, hitTestEdgeInsets)
        return hitFrame.contains(point)
    }
 
}

class NavigationButton: UIButton {
    override public func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let relativeFrame = self.bounds
        let hitTestEdgeInsets = UIEdgeInsetsMake(-18, -18, -18, -18)
        let hitFrame = UIEdgeInsetsInsetRect(relativeFrame, hitTestEdgeInsets)
        return hitFrame.contains(point)
    }
}

class TabBarVC: UITabBar {
    
    override func awakeFromNib() {
        
        // make unselected icons
        self.unselectedItemTintColor = UIColor.secondaryColor
    }
}
