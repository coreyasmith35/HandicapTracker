//
//  UIViewControllerExt.swift
//  GolfHandicap
//
//  Created by Corey Smith on 8/30/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentDetail(_ viewControlerToPresent: UIViewController) {
        let transistoin = CATransition()
        transistoin.duration = 0.3
        transistoin.type = kCATransitionPush
        transistoin.subtype = kCATransitionFromRight
        self.view.window?.layer.add(transistoin, forKey: kCATransition)
        
        present(viewControlerToPresent, animated: false, completion: nil)
    }
    func presentSecondary(_ viewControllerToPresent: UIViewController){
        let transistoin = CATransition()
        transistoin.duration = 0.3
        transistoin.type = kCATransitionPush
        transistoin.subtype = kCATransitionFromRight
        
        guard let presentedViewController = presentedViewController else {return}
        
        presentedViewController.dismiss(animated: false) {
            self.view.window?.layer.add(transistoin, forKey: kCATransition)
            self.present(viewControllerToPresent, animated: false,completion: nil)
        }
    }
    
    func dismissDetail() {
        let transistoin = CATransition()
        transistoin.duration = 0.3
        transistoin.type = kCATransitionPush
        transistoin.subtype = kCATransitionFromLeft
        self.view.window?.layer.add(transistoin, forKey: kCATransition)
        
        dismiss(animated: false, completion: nil)
    }
}
