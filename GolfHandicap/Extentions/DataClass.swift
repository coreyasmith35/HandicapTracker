//
//  DataClass.swift
//  GolfHandicap
//
//  Created by Corey Smith on 9/9/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import Foundation
import CoreData
/*
class Data {
    static var sharedInstance = 12 // This is singleton
    static var rounds: [Round] = []
    
    //static var handicap: Double
    
    static var scoreSorted: [Round] = []
    static var differintalSorted: [Round] = []
    static var dateSorted: [Round] = []
    
    static func update() {
        self.fetchCoreDataObjects()
        
        Data.scoreSorted = Data.rounds.sorted(by: { $0.score < $1.score})
        Data.differintalSorted = Data.rounds.sorted(by: { $0.differential < $1.differential})
        Data.dateSorted = Data.rounds.sorted(by: { $0.date! < $1.date!})
    }
    private init() {
        
    }
    
    static func fetchCoreDataObjects() {
        self.fetch { (complete) in
            if complete {
                if Data.rounds.count >= 1 {
                    //tableView.isHidden = false
                }else {
                    //tableView.isHidden = true
                }
            }
        }
    }
    
    static func removeRound(atIndexPath indexPath: IndexPath) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        managedContext.delete(Data.rounds[indexPath.row])
        
        do{
            try managedContext.save()
            //print("sucessfuly remvoed goal")
        } catch{
            debugPrint("Could not Remove: \(error.localizedDescription)")
        }
        
    }
    
    //fetch round data and save into an array
    static func fetch(completion:(_ complete: Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        
        let fetchRequest = NSFetchRequest<Round>(entityName: "Round")
        
        do{
            Data.rounds = try managedContext.fetch(fetchRequest)
            Data.rounds.sort(by: { $0.date! > $1.date!})
            //print("Successfully fetchded data!")
            completion(true)
        } catch {
            debugPrint("Could not fetch: \(error.localizedDescription)")
            completion(false)
        }
    }
 
}
*/
