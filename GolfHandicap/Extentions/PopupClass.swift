//
//  popupExt.swift
//  GolfHandicap
//
//  Created by Corey Smith on 9/10/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit

class Popup: UIView {
    override func awakeFromNib() {
        layer.cornerRadius = 15
        layer.shadowOpacity = 0.3
    }
}
