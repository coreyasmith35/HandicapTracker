//
//  ColorEnum.swift
//  GolfHandicap
//
//  Created by Corey Smith on 9/9/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit

extension UIColor {
    static let primaryColor = #colorLiteral(red: 0, green: 0.6205010414, blue: 0.33470577, alpha: 1)
    static let secondaryColor = #colorLiteral(red: 0.4933853149, green: 0.7736650109, blue: 0.4616621733, alpha: 1)
    static let tertiaryColor = #colorLiteral(red: 0.3125955087, green: 0.4966740359, blue: 1, alpha: 1)
    static let grayText = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
}

