//
//  GeneralInfoVC.swift
//  GolfHandicap
//
//  Created by Corey Smith on 9/11/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit

class GeneralInfoVC: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        writeText()
        textView.contentOffset = .zero
        // Do any additional setup after loading the view.
    }

    @IBAction func backButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    
    func writeText() {
        textView.text = "Disclaimer\n\tThis app is in no way affiliated with the USGA and the calculations provided by this app are not valid with the USGA. Handicap Tracker is for personal use and is no substitute for tracking your handicap directly through the USGA.\n\nFive Score Minimum\n\tHandicap Tracker will display your predicted handicap but it is not a equivalently valid handicap until you have at least 5 scores recorded to meet USGA standards.\n\nEquitable Stroke Control (ESC)\n\tHandicap Tracker utilizes the same formula as the USGA therefore in order to represent a player's potential ability you will need to adjust your score in accordents to USGA's Equitable Stroke Control. ---\n\nHandicap            Maximum Number\n\t9 or less         Double Bogey\n\t10-19              7\n\t20-29             8\n\t30-39             9\n\t40+                10\n\t\n\nDefinitions:\nGolf handicap - is a numerical measure of a golfer's potential ability.\nCourse Rating - is the scratch yardage of the course with the added difficulty provided by the unique obstacles of the course that would affect a scratch golfer (typically between 65-75).\nBogey Rating - is the bogey yardage of the course with the added difficulty provided by the unique obstacles of the course that would affect a bogey golfer.\nSlope Rating - is the  Bogey Rating minus Course Rating multiplied by 5.381 for men and 4.24 for women (55max-155min).\n\nMaximum Handicap\n\tThe maximum Handicap Index is 36.4 for men and 40.4 for women.\n"
    }
}
