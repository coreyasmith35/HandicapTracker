//
//  EditRoundVC.swift
//  GolfHandicap
//
//  Created by Corey Smith on 8/24/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit
import CoreData
import Firebase

protocol communicationControllerEdit {
    func backFromEdit()
}

class EditRoundVC: UIViewController{
    
    
    @IBOutlet weak var courseNameLable: UITextField!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var slopeLable: UITextField!
    @IBOutlet weak var ratingLable: UITextField!
    @IBOutlet weak var scoreLable: UITextField!
    @IBOutlet weak var addRoundButton: UIButton!
    
    var dateSelected: Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addRoundButton.setSelectedColor()
        let index = Int(editIndexPath.row)
        dateButton.setTitle(convertDate(date: rounds[index].date!), for: .normal)
        dateSelected = rounds[index].date
        courseNameLable.text = rounds[index].course
        slopeLable.text = String(describing: rounds[index].slope)
        ratingLable.text = String(describing: rounds[index].rating)
        scoreLable.text = String(describing: rounds[index].score)
        //print(rounds[index])
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    
    
    @IBAction func dateButtonPressed(_ sender: Any) {
        let datePicker = DatePickerDialog(textColor: .black,
                                          buttonColor: UIColor.primaryColor,
                                          font: UIFont.boldSystemFont(ofSize: 17),
                                          showCancelButton: true)
        datePicker.show("Date",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        minimumDate: nil,
                        maximumDate: nil,
                        datePickerMode: .date) { (date) in
                            if let dt = date {
                                //let formatter = DateFormatter()
                                //formatter.dateStyle = formatter.Style.long
                                //self.textField.text = formatter.string(from: dt)
                                self.dateButton.setTitle(self.convertDate(date: dt), for: .normal)
                                self.dateSelected = dt
                            }
        }
        
    }
    
    @IBAction func textFieldChanged(_ sender: Any) {
        _ = checkUserInput()
    }
    
    
    func convertDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        let currentDate = date
        dateFormatter.dateStyle = DateFormatter.Style.long
        let convertedDate = dateFormatter.string(from: currentDate as Date)
        return convertedDate
    }
    
    func checkUserInput() -> Bool {
        var check = true
        
        let scanner: Scanner = Scanner(string: ratingLable.text!)
        let isNumeric = scanner.scanDecimal(nil) && scanner.isAtEnd
        
        if !isNumeric {
            ratingLable.text = ""
        }
        
        if Double(ratingLable.text!) == nil {
            if (ratingLable.text?.hasSuffix("."))! {
                return false
            } else {
                ratingLable.text = ""
                return false
            }
        }
        
        if courseNameLable.text == ""{
            addRoundButton.setDeselectedColor()
            check = false
        } else {
            courseNameLable.textColor = UIColor.black
        }
        if slopeLable.text == "" || Int(slopeLable.text!)! < 55 || Int(slopeLable.text!)! > 155 {
            addRoundButton.setDeselectedColor()
            check = false
        } else {
            slopeLable.textColor = UIColor.black
        }
        if ratingLable.text == "" || Double(ratingLable.text!)! < 55 || Double(ratingLable.text!)! > 155 {
            addRoundButton.setDeselectedColor()
            check = false
        } else{
            ratingLable.textColor = UIColor.black
        }
        if scoreLable.text == "" || Int(scoreLable.text!)! < 0 || Int(scoreLable.text!)! > 200 {
            addRoundButton.setDeselectedColor()
            check = false
        } else {
            scoreLable.textColor = UIColor.black
        }
        
        if check {
            addRoundButton.setSelectedColor()
            //print("checkUserInput() retunred true")
            return true
        } else {
            return false
        }
    }
    
    func markUncompletedUserInput() -> Bool {
        var check = true
        if courseNameLable.text == ""{
            courseNameLable.attributedPlaceholder = NSAttributedString(string: courseNameLable.placeholder!, attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5157091618, green: 0.08794223517, blue: 0.08502068371, alpha: 0.7446196934)])
            check = false
        }
        if slopeLable.text == "" || Int(slopeLable.text!)! < 55 || Int(slopeLable.text!)! > 155 {
            slopeLable.attributedPlaceholder = NSAttributedString(string: slopeLable.placeholder!, attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5157091618, green: 0.08794223517, blue: 0.08502068371, alpha: 0.7446196934)])
            slopeLable.textColor = #colorLiteral(red: 0.7100027204, green: 0.09870325774, blue: 0.08507516235, alpha: 1)
            check = false
        }
        if ratingLable.text == "" || Double(ratingLable.text!)! < 55 || Double(ratingLable.text!)! > 85 {
            ratingLable.attributedPlaceholder = NSAttributedString(string: ratingLable.placeholder!, attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5157091618, green: 0.08794223517, blue: 0.08502068371, alpha: 0.7446196934)])
            ratingLable.textColor = #colorLiteral(red: 0.7100027204, green: 0.09870325774, blue: 0.08507516235, alpha: 1)
            check = false
        }
        if scoreLable.text == "" || Int(scoreLable.text!)! < 0 || Int(scoreLable.text!)! > 200 {
            scoreLable.attributedPlaceholder = NSAttributedString(string: scoreLable.placeholder!, attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5157091618, green: 0.08794223517, blue: 0.08502068371, alpha: 0.7446196934)])
            scoreLable.textColor = #colorLiteral(red: 0.7100027204, green: 0.09870325774, blue: 0.08507516235, alpha: 1)
            check = false
        }
        if check {
            //print("markUncompletedUserInput() retunred true")
            return true
        } else {
            return false
        }
        
    }
    
    @IBAction func addRoundButtonPressed(_ sender: UIButton) {
        //print("addRoundButtonPressed!!!!")
        if checkUserInput() {
            if markUncompletedUserInput() {
                /*
                //removeRound(atIndexPath: editIndexPath)
                self.save { (complete) in
                    if complete {
                        //--------------------------------
                        func fetch(completion:(_ complete: Bool) -> ()) {
                            guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
                            
                            let fetchRequest = NSFetchRequest<Round>(entityName: "Round")
                            
                            do{
                                rounds = try managedContext.fetch(fetchRequest)
                                rounds.sort(by: { $0.date! < $1.date!})
                                //print("Successfully fetchded data!")
                                completion(true)
                            } catch {
                                debugPrint("Could not fetch: \(error.localizedDescription)")
                                completion(false)
                            }
                        }
                        //--------------------------------
 
                 
                    }
                }*/
                let score = Double(scoreLable.text!)!
                let rating =  Double(ratingLable.text!)!
                let slope = Double(slopeLable.text!)!
                
                rounds[editIndexPath.row].course = courseNameLable.text
                rounds[editIndexPath.row].rating = rating
                rounds[editIndexPath.row].score = Int32(score)
                rounds[editIndexPath.row].slope = Int32(slope)
                rounds[editIndexPath.row].date = dateSelected
                let dif = (((score - rating) * 113 / slope) * 100).rounded() / 100
                rounds[editIndexPath.row].differential = dif
                
                guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
                //let round = Round(context: managedContext)
                
                do{
                    try managedContext.save()
                    
                } catch {
                    debugPrint("Could not save \(error.localizedDescription)")
                }
                dismiss(animated: true, completion: nil)
            } else {
                let generator = UINotificationFeedbackGenerator()
                generator.notificationOccurred(.error)
            }
        }else{
            //print("checkUserInput returned False...")
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.error)
            sender.shake()
            _ = markUncompletedUserInput()
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        //do stuff
        dismiss(animated: true, completion: nil)
    }
    

    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func removeRound(atIndexPath indexPath: IndexPath) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        managedContext.delete(rounds[indexPath.row])
        
        do{
            try managedContext.save()
            //updateData()
            //print("sucessfuly remvoed goal")
        } catch{
            debugPrint("Could not Remove: \(error.localizedDescription)")
        }
        
    }
    
    func save(completion: (_ finished: Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        let round = Round(context: managedContext)
        
        let score = Double(scoreLable.text!)!
        let rating =  Double(ratingLable.text!)!
        let slope = Double(slopeLable.text!)!
        
        round.course = courseNameLable.text
        round.rating = rating
        round.score = Int32(score)
        round.slope = Int32(slope)
        round.date = dateSelected
        let dif = (((score - rating) * 113 / slope) * 100).rounded() / 100
        round.differential = dif
        
        
        
        
    }
}
