//
//  FAQVC.swift
//  GolfHandicap
//
//  Created by Corey Smith on 9/11/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit

class FAQVC: UIViewController {

    @IBOutlet weak var FAQTextFeild: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        writeText()
        FAQTextFeild.contentOffset = .zero
        // Do any additional setup after loading the view.
    }

    @IBAction func backButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    func writeText() {
        
        FAQTextFeild.text = "Q. Where can I find the slope and rating for a course.\nA. The Slope and Rating of a course can be found on the score card or ncrdb.usga.org.\n\nQ. How do I calaculate Adjusted Gross Score?\nA. Adjusted Gross Score is simply your score with a max double bogey.\n\nQ. How is the handicap calculated?\nA. 1) Calculate Handicap Differential for each score by:\n\n\tHandicap Differential = (Adjusted Gross Score - Course Rating) X 113 ÷ Slope Rating\n\n2) Find the average handicap differential:\n\n\t Select 10 lowest Handicap Differentials from the 20 most recent scores, add them together and divide by 10. If there are less than 20 rounds recorded the see chart below.\n\n\tRounds\t   Differentials Used\n\t5-6         Lowest 1\n\t7-8         Lowest 2\n\t9-10       Lowest 3\n\t11-12      Lowest 4\n\t13-14     Lowest 5\n\t15-16      Lowest 6\n\t17            Lowest 6\n\t18            Lowest 8\n\t19            Lowest 9\n\t20            Lowest 10\n\n3) Finally multiply the average differential by 96%\n\nQ. Why does my handicap say Predicted Handicap?\nA. It is refering to the fact that you have less than 5 rounds recorded thus making your handicap not applicable per USGA standards.\n\n "
    }
}
