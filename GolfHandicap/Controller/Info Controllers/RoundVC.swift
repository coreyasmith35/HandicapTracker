//
//  RoundVC.swift
//  HandicapTracker
//
//  Created by Corey Smith on 10/10/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit

class RoundVC: UIViewController {

    @IBOutlet weak var courseLable: UILabel!
    @IBOutlet weak var dateLable: UILabel!
    @IBOutlet weak var ratingLable: UILabel!
    @IBOutlet weak var slopeLable: UILabel!
    @IBOutlet weak var scoreLable: UILabel!
    @IBOutlet weak var differentialLable: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        let index = Int(editIndexPath.row)
        courseLable.text = rounds[index].course
        dateLable.text = convertDate(date: rounds[index].date!)
        courseLable.text = rounds[index].course
        slopeLable.text = String(describing: rounds[index].slope)
        ratingLable.text = String(describing: rounds[index].rating)
        scoreLable.text = String(describing: rounds[index].score)
        differentialLable.text = String(describing: rounds[index].differential)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func convertDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        let currentDate = date
        dateFormatter.dateStyle = DateFormatter.Style.long
        let convertedDate = dateFormatter.string(from: currentDate as Date)
        return convertedDate
    }
}
