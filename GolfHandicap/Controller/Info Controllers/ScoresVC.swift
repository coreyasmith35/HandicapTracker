//
//  ScoresVC.swift
//  SecondViewController.swift
//  GolfHandicap
//
//  Created by Corey Smith on 8/23/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import hkMotus

let appDelegate = UIApplication.shared.delegate as? AppDelegate

var rounds: [Round] = []
var filterdRounds: [Round] = []
var adHasBeenDisplayed = false
var editIndexPath: IndexPath = [0,0]
var inSearchMode = false
var didAnimate = false

class ScoresVC: UIViewController, UISearchBarDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortDateButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = false
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        if #available(iOS 10.0, *) {
            searchBar.scopeButtonTitles = nil
            searchBar.showsScopeBar = false
            searchBar.barTintColor = UIColor.secondaryColor
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        endSearch()
        updateData()
    }
    
    
    @IBAction func addRoundButtonPressed(_ sender: Any) {
        endSearch()
        guard let addRoundVC = storyboard?.instantiateViewController(withIdentifier: "AddRoundVC") else {return}
        presentDetail(addRoundVC)
    }
    
    @IBAction func exportButtonPressed(_ sender: Any) {
        endSearch()
        let fileName = "HandicapTracker.csv"
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        
        var csvText = "Handicap Tracker\n\nHandicap:,\(handicap)\n\nDate,Course,Rating,Slope, ,Differential,Score\n"
        
        let cvcData = rounds.sorted(by: { $0.date! > $1.date!})
        
        for data in cvcData {
            
            let currentDate = data.date!
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let convertedDate = dateFormatter.string(from: currentDate as Date)
            let newLine = "\(convertedDate),\(data.course!),\(data.rating),\(data.slope), ,\(data.differential),\(data.score)\n"
            csvText.append(newLine)
        }
        
        do {
            try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("Failed to create file")
            print("\(error)")
        }
        let vc = UIActivityViewController(activityItems: [path!], applicationActivities: [])
        present(vc, animated: true, completion: nil)
    }
    
    func updateData() {
        fetchCoreDataObjects()
        if let sort = UserDefaults.standard.object(forKey: "sortDefalt") as? String{
            switch sort {
            case "date":
                break
            case "score":
                sortByScore()
                break
            case "differential":
                sortByDifferential()
                break
            default:
                print("Error: in defalt")
                break
            }
            
            if !didAnimate{
                tableView.reloadData(effect: .LeftAndRight)
                didAnimate = true
            } else{
                tableView.reloadData()
            }
        } else {
            //user defalt not set
            if !didAnimate{
                tableView.reloadData(effect: .LeftAndRight)
                didAnimate = true
            } else{
                tableView.reloadData()
            }
        }
        
    }
    
    func fetchCoreDataObjects() {
        self.fetch { (complete) in
            if complete {
                if rounds.count >= 1 {
                    tableView.isHidden = false
                }else {
                    tableView.isHidden = true
                }
            }
        }
    }
 
    @IBAction func sortDateButtonPressed(_ sender: Any) {
        showActionSheet()
    }
    
    func showActionSheet() {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let date = UIAlertAction(title: "Date", style: .default) { action in
            UserDefaults.standard.set("date", forKey: "sortDefalt")
            self.sortByDate()
        }
        
        let score = UIAlertAction(title: "Score", style: .default) { action in
            UserDefaults.standard.set("score", forKey: "sortDefalt")
            self.sortByScore()
        }
        
        let differential = UIAlertAction(title: "Differential", style: .default) { action in
            UserDefaults.standard.set("differential", forKey: "sortDefalt")
            self.sortByDifferential()
        }
        
        actionSheet.addAction(date)
        actionSheet.addAction(score)
        actionSheet.addAction(differential)
        actionSheet.addAction(cancel)
        
        present(actionSheet, animated: true, completion: nil)
    }
    func sortByDate(){
        rounds.sort(by: { $0.date! > $1.date!})
        tableView.reloadData(effect: .LeftAndRight)
    }
    func sortByDifferential() {
        rounds.sort(by: { $0.differential < $1.differential})
        tableView.reloadData(effect: .LeftAndRight)
    }
    
    func sortByScore() {
        rounds.sort(by: { $0.score < $1.score})
        tableView.reloadData(effect: .LeftAndRight)
        
    }
}


extension ScoresVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inSearchMode {
            return filterdRounds.count
        }else {
            return rounds.count
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "roundCell") as? RoundCell else { return UITableViewCell()}

        let round = rounds[indexPath.row]
        if inSearchMode {
            cell.configureCell(round: filterdRounds[indexPath.row])
        } else {
            cell.configureCell(round: round)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    /*
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "DELETE", handler: { (rowAction, indexPath) in
            print("editActionForRowAt")
            self.removeRound(atIndexPath: indexPath)
            self.fetchCoreDataObjects()
            tableView.deleteRows(at: [indexPath], with: .automatic)
        })
        
        deleteAction.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        return [deleteAction]
    }
 */
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            if inSearchMode {
                let round = filterdRounds[indexPath.row]
                var removeIndexPath:IndexPath = [0,0]
                for i in 0..<rounds.count {
                    if round == rounds[i] {
                        removeIndexPath = [0,i]
                    } else {
                        //print("Error: Can not find Round and delete round.")
                    }
                }
                self.removeRound(atIndexPath: removeIndexPath)
                self.fetchCoreDataObjects()
                let lower = searchBar.text!
                filterdRounds = rounds.filter({$0.course?.range(of: lower) != nil})
                tableView.deleteRows(at: [indexPath], with: .automatic)
            } else {
                self.removeRound(atIndexPath: indexPath)
                self.fetchCoreDataObjects()
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if inSearchMode {
            let round = filterdRounds[indexPath.row]
            for i in 0..<rounds.count {
                if round == rounds[i] {
                    editIndexPath = [0,i]
                } else {
                    print("Error: Can not find Round.")
                }
            }
            
        } else {
            editIndexPath = indexPath
            //print(indexPath)
        }
        
        guard let editRoundVC = storyboard?.instantiateViewController(withIdentifier: "RoundVC") else {return}
        presentDetail(editRoundVC)
        //print(editIndexPath.row)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        inSearchMode = true
        //Change navigatrion bar sort button
        sortDateButton.isHidden = true
        //UIView.animate(withDuration: Double(0.5), animations: {
            self.searchBarConstraint.constant = -80
            self.view.layoutIfNeeded()
        //})
        searchBar.setShowsCancelButton(true, animated: true)
        let lower = searchBar.text!
        filterdRounds = rounds.filter({$0.course?.range(of: lower) != nil})
        tableView.reloadData()
        
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        endSearch()
    }
    
    func endSearch(){
        inSearchMode = false
        tableView.reloadData()
        sortDateButton.isHidden = false
        //UIView.animate(withDuration: Double(0.5), animations: {
            self.searchBarConstraint.constant = 0
            self.view.layoutIfNeeded()
        //})
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = ""
        view.endEditing(true)
    }
}

extension ScoresVC {
    
    
    func removeRound(atIndexPath indexPath: IndexPath) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        managedContext.delete(rounds[indexPath.row])
        
        do{
            try managedContext.save()
            //updateData()
            //print("sucessfuly remvoed goal")
        } catch{
            debugPrint("Could not Remove: \(error.localizedDescription)")
        }
        
    }
    
    //fetch round data and save into an array
    func fetch(completion:(_ complete: Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        
        let fetchRequest = NSFetchRequest<Round>(entityName: "Round")
        
        do{
            rounds = try managedContext.fetch(fetchRequest)
            rounds.sort(by: { $0.date! > $1.date!})
            //print("Successfully fetchded data!")
            completion(true)
        } catch {
            debugPrint("Could not fetch: \(error.localizedDescription)")
            completion(false)
        }
    }
 
}
