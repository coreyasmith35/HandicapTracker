//
//  InfoVC.swift
//  GolfHandicap
//
//  Created by Corey Smith on 9/10/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit
import MessageUI

class InfoVC: UIViewController, UITableViewDelegate, MFMailComposeViewControllerDelegate  {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func twitterButtonPressed(_ sender: Any) {
        let screenName =  "handicaptracker"
        let appURL = NSURL(string: "twitter://user?screen_name=\(screenName)")!
        let webURL = NSURL(string: "https://twitter.com/\(screenName)")!
        
        //if application.open(appURL as URL, options: [:], completionHandler: nil )
        UIApplication.shared.open(appURL as URL, options: [:],
        completionHandler: {
        (success) in
            if !success {
                UIApplication.shared.open(webURL as URL, options: [:], completionHandler: nil)
            }
        })
        
    }
    
    @IBAction func emailButtonPressed(_ sender: Any) {
        sendEmail()
    }
    @IBAction func backButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)

    }

    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["HandicapTrackerApp@gmail.com"])
            //mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            let alertController = UIAlertController(title: "Error", message:
                "Could not open mail app.", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}




