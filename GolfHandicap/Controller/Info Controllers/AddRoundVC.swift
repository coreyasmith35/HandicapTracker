//
//  AddRoundVC.swift
//  GolfHandicap
//
//  Created by Corey Smith on 8/24/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import SearchTextField

class AddRoundVC: UIViewController, GADInterstitialDelegate{
    
    

    @IBOutlet weak var courseNameLable: SearchTextField!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var slopeLable: UITextField!
    @IBOutlet weak var ratingLable: UITextField!
    @IBOutlet weak var scoreLable: UITextField!
    @IBOutlet weak var addRoundButton: UIButton!
    
    var dateSelected: Date!
    var interstitial: GADInterstitial!
    var courseList: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateSelected = Date()
        dateButton.setTitle(convertDate(date: dateSelected), for: .normal)
        
        //init google ads
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-5749448049304120/6779385481")
        
        if !adHasBeenDisplayed {
            let request = GADRequest()
            //testing ad --------->
            request.testDevices = [ kGADSimulatorID,"2077ef9a63d2b398840261c8221a0c9b" ];  // Sample device ID
            //testing ad --------->
            interstitial.load(request)
        }
        interstitial.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getFilterdList()
        // Modify current theme properties
        courseNameLable.theme.font = UIFont.systemFont(ofSize: 15)
        courseNameLable.theme.bgColor = UIColor.white
        //courseNameLable.theme.borderColor = UIColor.gray
        //courseNameLable.theme.separatorColor = UIColor.gray
        courseNameLable.theme.fontColor = UIColor.primaryColor
        courseNameLable.theme.cellHeight = 50
        
        
        // Set the max number of results. By default it's not limited
        courseNameLable.maxNumberOfResults = 5
        
        // Show the list of results as soon as the user makes focus - Default: false
        courseNameLable.startVisible = true
    }
    
    @IBAction func helpButtonPressed(_ sender: Any) {
        //help button
    }
    
    @IBAction func dateButtonPressed(_ sender: Any) {
        let datePicker = DatePickerDialog(textColor: .black,
                                          buttonColor: UIColor.primaryColor,
                                          font: UIFont.boldSystemFont(ofSize: 17),
                                          showCancelButton: true)
        datePicker.show("Date",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        minimumDate: nil,
                        maximumDate: nil,
                        datePickerMode: .date) { (date) in
                            if let dt = date {
//                                let formatter = DateFormatter()
//                                formatter.dateStyle = formatter.Style.long
                                //self.textField.text = formatter.string(from: dt)
                                self.dateButton.setTitle(self.convertDate(date: dt), for: .normal)
                                self.dateSelected = dt
                            }
        }
    
    }
    
    @IBAction func textFeildChanged(_ sender: Any) {
        _ = checkUserInput()
    }
    
    func getFilterdList(){
        //Gets the users courses and removes the duplecats so they can be selected from drop down
        for i in 0 ..< rounds.count {
            //print(i,rounds[i].course!)
            courseList.append(rounds[i].course!)
        }
        var encountered = Set<String>()
        var result: [String] = []
        for value in courseList {
            if encountered.contains(value) {
                // Do not add a duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        courseNameLable.filterStrings(result)
        
        courseNameLable.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            //print("Item at position \(itemPosition): \(item.title)")
            
            // Do whatever you want with the picked item
            //print("Selected item:",item.title)
            self.courseNameLable.text = item.title
            
            loop: for i in 0 ..< rounds.count {
                if rounds[i].course == item.title {
                    self.slopeLable.text = String(describing: rounds[i].slope)
                    self.ratingLable.text = String(describing: rounds[i].rating)
                    break loop
                }
            }
        }
    }
    
    
    func convertDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        let currentDate = date
        dateFormatter.dateStyle = DateFormatter.Style.long
        let convertedDate = dateFormatter.string(from: currentDate as Date)
        return convertedDate
    }
    
    func checkUserInput() -> Bool {
        var check = true
        
        let scanner: Scanner = Scanner(string: ratingLable.text!)
        let isNumeric = scanner.scanDecimal(nil) && scanner.isAtEnd
        
        if !isNumeric {
            ratingLable.text = ""
        }

        if Double(ratingLable.text!) == nil {
            if (ratingLable.text?.hasSuffix("."))! {
                return false
            } else {
                ratingLable.text = ""
                return false
            }
        }

        if courseNameLable.text == ""{
            addRoundButton.setDeselectedColor()
            check = false
        } else {
            courseNameLable.textColor = UIColor.black
        }
        if slopeLable.text == "" || Int(slopeLable.text!)! < 55 || Int(slopeLable.text!)! > 155 {
            addRoundButton.setDeselectedColor()
            check = false
        } else {
            slopeLable.textColor = UIColor.black
        }
        if ratingLable.text == "" || Double(ratingLable.text!)! < 55 || Double(ratingLable.text!)! > 155 {
            addRoundButton.setDeselectedColor()
            check = false
        } else{
            ratingLable.textColor = UIColor.black
        }
        if scoreLable.text == "" || Int(scoreLable.text!)! < 0 || Int(scoreLable.text!)! > 200 {
            addRoundButton.setDeselectedColor()
            check = false
        } else {
            scoreLable.textColor = UIColor.black
        }
        
        if check {
            addRoundButton.setSelectedColor()
            //print("checkUserInput() retunred true")
            return true
        } else {
            return false
        }
    }
    
    func markUncompletedUserInput() -> Bool {
        var check = true
        if courseNameLable.text == ""{
            courseNameLable.attributedPlaceholder = NSAttributedString(string: courseNameLable.placeholder!, attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5157091618, green: 0.08794223517, blue: 0.08502068371, alpha: 0.7446196934)])
            check = false
        }
        if slopeLable.text == "" || Int(slopeLable.text!)! < 55 || Int(slopeLable.text!)! > 155 {
            slopeLable.attributedPlaceholder = NSAttributedString(string: slopeLable.placeholder!, attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5157091618, green: 0.08794223517, blue: 0.08502068371, alpha: 0.7446196934)])
            slopeLable.textColor = #colorLiteral(red: 0.7100027204, green: 0.09870325774, blue: 0.08507516235, alpha: 1)
            check = false
        }
        if ratingLable.text == "" || Double(ratingLable.text!)! < 55 || Double(ratingLable.text!)! > 85 {
            ratingLable.attributedPlaceholder = NSAttributedString(string: ratingLable.placeholder!, attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5157091618, green: 0.08794223517, blue: 0.08502068371, alpha: 0.7446196934)])
            ratingLable.textColor = #colorLiteral(red: 0.7100027204, green: 0.09870325774, blue: 0.08507516235, alpha: 1)
            check = false
        }
        if scoreLable.text == "" || Int(scoreLable.text!)! < 0 || Int(scoreLable.text!)! > 200 {
            scoreLable.attributedPlaceholder = NSAttributedString(string: scoreLable.placeholder!, attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5157091618, green: 0.08794223517, blue: 0.08502068371, alpha: 0.7446196934)])
            scoreLable.textColor = #colorLiteral(red: 0.7100027204, green: 0.09870325774, blue: 0.08507516235, alpha: 1)
            check = false
        }
        if check {
            //print("markUncompletedUserInput() retunred true")
            return true
        } else {
            return false
        }
        
    }
    
    @IBAction func addRoundButtonPressed(_ sender: UIButton) {
        //print("addRoundButtonPressed!!!!")
        if checkUserInput() {
            if markUncompletedUserInput() {
                self.save { (complete) in
                    if complete {
                        //print("data was saved suscessfully")
                        if !adHasBeenDisplayed {
                            //display ad
                            if interstitial.isReady {
                                interstitial.present(fromRootViewController: self)
                            } else {
                                //print("Ad wasn't ready")
                                //ad was not ready or in window
                                dismiss(animated: true, completion: nil)
                            }
                        } else {
                            //ad has already been displayd once
                            dismiss(animated: true, completion: nil)
                        }
                    }
                }
            } else {
                let generator = UINotificationFeedbackGenerator()
                generator.notificationOccurred(.error)
            }
        }else{
            //print("checkUserInput returned False...")
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.error)
            sender.shake()
            _ = markUncompletedUserInput()
        }
    }
    

    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        //print("ad did dismiss screen")
        adHasBeenDisplayed = true
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func save(completion: (_ finished: Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        let round = Round(context: managedContext)
        
        let score = Double(scoreLable.text!)!
        let rating =  Double(ratingLable.text!)!
        let slope = Double(slopeLable.text!)!
        
        round.course = courseNameLable.text
        round.rating = rating
        round.score = Int32(score)
        round.slope = Int32(slope)
        round.date = dateSelected
        let dif = (((score - rating) * 113 / slope) * 100).rounded() / 100
        round.differential = dif
        //print(round.differential)
       
        
        do{
            try managedContext.save()
            completion(true)
            //print("Sucsessfuly saved date!")
        } catch {
            debugPrint("Could not save \(error.localizedDescription)")
            completion(false)
        }
    }
}
