//
//  ProfileVC.swift
//  FirstViewController.swift
//  GolfHandicap
//
//  Created by Corey Smith on 8/23/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit
import CoreData
import Charts

var handicap: Double = 0

class ProfileVC: UIViewController {
    
    //Handicap Lable
    @IBOutlet weak var handicapNameLable: UILabel!
    @IBOutlet weak var handicapLable: UILabel!
    
    //Lowest Score Lables
    @IBOutlet weak var lowestCourseLable: UILabel!
    @IBOutlet weak var lowestSlopeLable: UILabel!
    @IBOutlet weak var lowestRatingLable: UILabel!
    @IBOutlet weak var lowestDateLable: UILabel!
    @IBOutlet weak var lowestScoreLable: UILabel!
    @IBOutlet weak var lowestDifferentialLable: UILabel!
    
    //Rounds Logged Lable
    @IBOutlet weak var roundsLoggedLable: UILabel!
    @IBOutlet weak var coursesLoggedLable: UILabel!
    @IBOutlet weak var strokesLoggedLable: UILabel!
    
    
    //Graph View
    @IBOutlet weak var graphView: LineChartView!
    
    //UIViews Welcome Page and Working Page
    @IBOutlet weak var workingView: UIView!
    @IBOutlet weak var welcomeView: UIView!
    
    //PopUpView and Dissmiss button
    @IBOutlet weak var popUpView: Popup!
    
    var disclaimerHasBeenDisplayed = false
    var didAnimate = false
    
    //test vars for  Graph data
    var maxGraphPoints = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchCoreDataObjects()
        //graphView.dataSource = self
        //setupGraph(graphView: graphView)
        
        
    }
    
    @IBAction func addRoundButtonPressed(_ sender: Any) {
        guard let addRoundVC = storyboard?.instantiateViewController(withIdentifier: "AddRoundVC") else {return}
        presentDetail(addRoundVC)
    }
    
    @IBAction func popUpButtonPressed(_ sender: Any) {
        popUpView.isHidden = true
        disclaimerHasBeenDisplayed = true
    }
    
    func fetchCoreDataObjects() {
        self.fetch { (complete) in
            if complete {
                if rounds.count >= 1 {
                    //display working view and hide welcome view
                    calcData()
                    updateGraph()
                    workingView.isHidden = false
                    welcomeView.isHidden = true
                    if rounds.count <= 4 {
                        if disclaimerHasBeenDisplayed {
                            popUpView.isHidden = true
                        } else {
                            popUpView.isHidden = false
                            disclaimerHasBeenDisplayed = true
                        }
                    }
                } else {
                    //hide working view and display welcome View
                    workingView.isHidden = true
                    welcomeView.isHidden = false
                    
                }
            }
        }
    }
    
    //fetch round data and save into an array
    func fetch(completion:(_ complete: Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        
        let fetchRequest = NSFetchRequest<Round>(entityName: "Round")
        
        do{
            rounds = try managedContext.fetch(fetchRequest)
            rounds.sort(by: { $0.date! < $1.date!})
            //print("Successfully fetchded data!")
            completion(true)
        } catch {
            debugPrint("Could not fetch: \(error.localizedDescription)")
            completion(false)
        }
    }
    
    func calcData() {
        //Displays Number Of Rounds User Has
        roundsLoggedLable.text = String(describing: rounds.count)
        
        // Get total strokes and unique corses
        var uniqueCorses = [String]()
        var totalStrokes = 0
        for round in rounds {
            
            if !uniqueCorses.contains(round.course!){
                uniqueCorses.append(round.course!)
            }
            
            totalStrokes = totalStrokes + Int(round.score)
        }
        
        // Displays Number of Corses user has played
        coursesLoggedLable.text = String(describing: uniqueCorses.count)
        
        //Displays Number of Strokes user has hit
        strokesLoggedLable.text = String(describing: totalStrokes)
        
        //Sorts and Displays Lowest Score
        let scoreSorted = rounds.sorted(by: { $0.score < $1.score})
        configureLowestScore(round: scoreSorted[0])
        
        //Sorts and Displays Best Round by Differential
        let differintalSorted = rounds.sorted(by: { $0.differential < $1.differential})
        //configureBestRound(round: differintalSorted[0])
        
        //Uses differintalSorted to Calculate Handicap and Displays
        var handicapDifferentialAverage = 0.00
        
        switch differintalSorted.count {
        case 0:
            print("Error atempting to calculate handicap with no scores inputed")
        case 1, 2, 3, 4, 5, 6:
            //Using Lowest 1 Differential
            handicapDifferentialAverage = differintalSorted[0].differential
        case 7, 8:
            //Using Lowest 2 Differential
            handicapDifferentialAverage = (differintalSorted[0].differential + differintalSorted[1].differential) / 2
        case 9, 10:
            //Using Lowest 3 Differential
            for index in 0...3 {
                handicapDifferentialAverage = handicapDifferentialAverage + differintalSorted[index].differential
            }
            handicapDifferentialAverage = handicapDifferentialAverage / 3
        case 11, 12:
            //Using Lowest 4 Differential
            for index in 0...4 {
                handicapDifferentialAverage = handicapDifferentialAverage + differintalSorted[index].differential
            }
            handicapDifferentialAverage = handicapDifferentialAverage / 4
        case 13, 14:
            //Using Lowest 5 Differential
            for index in 0...5 {
                handicapDifferentialAverage = handicapDifferentialAverage + differintalSorted[index].differential
            }
            handicapDifferentialAverage = handicapDifferentialAverage / 5
        case 15, 16:
            //Using Lowest 6 Differential
            for index in 0...6 {
                handicapDifferentialAverage = handicapDifferentialAverage + differintalSorted[index].differential
            }
            handicapDifferentialAverage = handicapDifferentialAverage / 6
        case 17:
            //Using Lowest 7 Differential
            for index in 0...7 {
                handicapDifferentialAverage = handicapDifferentialAverage + differintalSorted[index].differential
            }
            handicapDifferentialAverage = handicapDifferentialAverage / 7
        case 18:
            //Using Lowest 8 Differential
            for index in 0...8 {
                handicapDifferentialAverage = handicapDifferentialAverage + differintalSorted[index].differential
            }
            handicapDifferentialAverage = handicapDifferentialAverage / 8
        case 19:
            //Using Lowest 9 Differential
            for index in 0...9 {
                handicapDifferentialAverage = handicapDifferentialAverage + differintalSorted[index].differential
            }
            handicapDifferentialAverage = handicapDifferentialAverage / 9
        case 20:
            //Using Lowest 10 Differential
            for index in 0...10 {
                handicapDifferentialAverage = handicapDifferentialAverage + differintalSorted[index].differential
            }
            handicapDifferentialAverage = handicapDifferentialAverage / 10
        default:
            let latestRounds = rounds[0...19]
            let latestDifferintalSorted = latestRounds.sorted(by: { $0.differential < $1.differential})
            for index in 0...10 {
                handicapDifferentialAverage = handicapDifferentialAverage + latestDifferintalSorted[index].differential
            }
            handicapDifferentialAverage = handicapDifferentialAverage / 10
        }
        //print("Handicap Differential Average: \(handicapDifferentialAverage)")
        handicap = (handicapDifferentialAverage * 0.96).truncate(places: 2)
        //print("Handicap: \(handicap)")
        //check to see if handicap is > than max handicap posible 36.4
        if handicap < 0 {
            handicapLable.text = "+ \(String(describing: abs(handicap)))"
        }else if handicap < 36.4 {
            handicapLable.text = String(describing: handicap)
        }else {
            handicapLable.text = String(describing: 36.4)
        }
        if rounds.count < 5 {
            handicapNameLable.text = "Predicted Handicap:"
        } else {
            handicapNameLable.text = "Handicap:"
        }
        if differintalSorted.count < 1 {
            print("Error atempting to calculate handicap with no scores inputed")
        }
        
    }
    
    func configureLowestScore(round: Round) {
        
        self.lowestCourseLable.text = round.course
        self.lowestSlopeLable.text = String(describing: round.slope)
        self.lowestRatingLable.text = String(describing: round.rating)
        self.lowestScoreLable.text = String(describing: round.score)
        let dateFormatter = DateFormatter()
        let currentDate = round.date
        dateFormatter.dateStyle = DateFormatter.Style.short
        let convertedDate = dateFormatter.string(from: (currentDate)!)
        self.lowestDateLable.text = convertedDate
        
        self.lowestDifferentialLable.text = String(describing: round.differential)
    }
    
    func updateGraph(){
        
        let graphData = rounds.map { $0.score }

        //this is the Array that will eventually be displayed on the graph.
        var lineChartEntry  = [ChartDataEntry]()
        
        var numPoints: Int
        if graphData.count > 20 {
            numPoints = 20
        } else {
            numPoints = graphData.count
        }
        
        for i in (graphData.count - numPoints)..<graphData.count {
            // here we set the X and Y status in a data chart entry
            let value = ChartDataEntry(x: Double(i), y: Double(graphData[i]))
            //print(value)
            lineChartEntry.append(value) // here we add it to the data set
        }
        
        //Here we convert lineChartEntry to a LineChartDataSet
        //name of data point color in ""
        let line1 = LineChartDataSet(values: lineChartEntry, label: "Score")
        
        //function colorering
        line1.colors = [NSUIColor.primaryColor] //Sets the colour to blue
        line1.setColors(NSUIColor.primaryColor)
        line1.circleColors = [NSUIColor.primaryColor]
        line1.fillColor = UIColor.secondaryColor
        line1.valueColors = [NSUIColor.primaryColor]
        line1.valueFont = UIFont.systemFont(ofSize: 12.0, weight: UIFont.Weight.regular)
        
        //gradient color
        // Colors of the gradient
        let gradientColors = [UIColor.primaryColor.cgColor, UIColor.secondaryColor.cgColor] as CFArray
        let colorLocations:[CGFloat] = [1.0, 0.0] // Positioning of the gradient
        let gradient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations) // Gradient Object
        line1.fill = Fill.fillWithLinearGradient(gradient!, angle: 90.0) // Set the Gradient
        line1.drawFilledEnabled = true
        
        
        // point size
        line1.circleHoleRadius = 3
        line1.circleRadius = 5
        
        //type of line
        line1.mode = .cubicBezier
        line1.cubicIntensity = 0.1 // Defalt: 0.2 (min = 0.05, max = 1)
        
        let data = LineChartData() //This is the object that will be added to the chart
        
        data.addDataSet(line1) //Adds the line to the dataSet
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .none
        formatter.maximumFractionDigits = 0
        formatter.multiplier = 1.0
        data.setValueFormatter(DefaultValueFormatter(formatter: formatter))
        
        graphView.data = data //finally - it adds the chart data to the chart and causes an update
        
        graphView.chartDescription?.enabled = false
        //graphView.chartDescription?.text = "Handicap Graph" // Here we set the description for the graph
        
        if !didAnimate{
            graphView.animate(xAxisDuration: 0.25)
            //graphView.animate(yAxisDuration: 0.5, easingOption: .easeInExpo)
            //graphView.animate(xAxisDuration: 1.0, easingOption: .easeInExpo)
            didAnimate = true
        }
        
        //stop graph from scrolling!!!!
        graphView.pinchZoomEnabled = true
        
        graphView.noDataTextColor = UIColor.primaryColor
        graphView.borderColor = UIColor.primaryColor
        graphView.leftAxis.gridColor = UIColor.primaryColor
        graphView.xAxis.axisLineColor = UIColor.primaryColor
        graphView.leftAxis.labelTextColor = UIColor.primaryColor
       
        graphView.leftAxis.enabled = true
        graphView.rightAxis.enabled = false
        graphView.xAxis.enabled = false
        graphView.leftAxis.spaceTop = 0.25
        graphView.leftAxis.spaceBottom = 0.25
        //graphView.setViewPortOffsets(left: 20.0, top: 0.0, right: 00.0, bottom: 0.0)
        
    }

}

