//
//  LowestScoreCell.swift
//  GolfHandicap
//
//  Created by Corey Smith on 9/5/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit

class LowestScoreCell: UICollectionViewCell {
    
    //Lowest Score Lables
    @IBOutlet weak var lowestCourseLable: UILabel!
    @IBOutlet weak var lowestSlopeLable: UILabel!
    @IBOutlet weak var lowestRatingLable: UILabel!
    @IBOutlet weak var lowestDateLable: UILabel!
    @IBOutlet weak var lowestScoreLable: UILabel!
    
    func configureCell(round: Round) {
        
        self.lowestCourseLable.text = round.course
        self.lowestSlopeLable.text = String(describing: round.slope)
        self.lowestRatingLable.text = String(describing: round.rating)
        self.lowestScoreLable.text = String(describing: round.score)
        let dateFormatter = DateFormatter()
        let currentDate = round.date
        dateFormatter.dateStyle = DateFormatter.Style.short
        let convertedDate = dateFormatter.string(from: (currentDate)!)
        self.lowestDateLable.text = convertedDate
        
    }
    
}
