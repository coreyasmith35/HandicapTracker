//
//  BestRoundCell.swift
//  GolfHandicap
//
//  Created by Corey Smith on 9/5/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit

class BestRoundCell: UICollectionViewCell {
    
    //Best Round Lables
    @IBOutlet weak var bestCourseLable: UILabel!
    @IBOutlet weak var bestSlopeLable: UILabel!
    @IBOutlet weak var bestRatingLable: UILabel!
    @IBOutlet weak var bestDateLable: UILabel!
    @IBOutlet weak var bestScoreLable: UILabel!
    
    func configureCell(round: Round) {
        
        self.bestCourseLable.text = round.course
        self.bestSlopeLable.text = String(describing: round.slope)
        self.bestRatingLable.text = String(describing: round.rating)
        self.bestScoreLable.text = String(describing: round.score)
        let dateFormatter = DateFormatter()
        let currentDate = round.date
        dateFormatter.dateStyle = DateFormatter.Style.short
        let convertedDate = dateFormatter.string(from: (currentDate)!)
        self.bestDateLable.text = convertedDate
        
    }
}
