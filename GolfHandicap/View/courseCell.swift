//
//  courseCell.swift
//  HandicapTracker
//
//  Created by Corey Smith on 3/19/18.
//  Copyright © 2018 Corey Smith. All rights reserved.
//

import UIKit

class CourseCell: UITableViewCell {
    
    @IBOutlet weak var courseNameLable: UILabel!
    @IBOutlet weak var slopeLable: UILabel!
    @IBOutlet weak var ratingLable: UILabel!
    @IBOutlet weak var courseHandicap: UILabel!
   
    
    
    
    func configureCell(round: Round) {
        self.courseNameLable.text = round.course
        self.slopeLable.text = "\(String(describing: round.rating))/\(String(describing: round.slope))"
        self.ratingLable.text = String(describing: round.differential)

    }
    
}

/*
Stats avalable
 - Name
 - Last played
 - total rounds
 - rating
 - slope
 - avg score
 - Strokes at course
 - Priviouse scores ui table
 - Lowest score at course
 - course handicap
*/

/*
Table View
 - Name
 - slope
 - rating
 - course handicap
 */
