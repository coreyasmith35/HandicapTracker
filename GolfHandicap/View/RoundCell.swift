//
//  RoundCell.swift
//  GolfHandicap
//
//  Created by Corey Smith on 8/24/17.
//  Copyright © 2017 Corey Smith. All rights reserved.
//

import UIKit

class RoundCell: UITableViewCell {
    
    @IBOutlet weak var courseNameLable: UILabel!
    @IBOutlet weak var slopeLable: UILabel!
    @IBOutlet weak var ratingLable: UILabel!
    @IBOutlet weak var scoreLable: UILabel!
    @IBOutlet weak var dateLable: UILabel!
    
    
   
    func configureCell(round: Round) {
        self.courseNameLable.text = round.course
        self.slopeLable.text = "\(String(describing: round.rating))/\(String(describing: round.slope))"
        self.ratingLable.text = String(describing: round.differential)
        self.scoreLable.text = String(describing: round.score)
        let dateFormatter = DateFormatter()
        let currentDate = round.date
        dateFormatter.dateStyle = DateFormatter.Style.short
        let convertedDate = dateFormatter.string(from: (currentDate)!)
        self.dateLable.text = convertedDate
    }
    
}
